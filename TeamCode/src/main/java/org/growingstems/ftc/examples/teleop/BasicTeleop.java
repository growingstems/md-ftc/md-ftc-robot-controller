package org.growingstems.ftc.examples.teleop;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@Disabled
@TeleOp(name = "Basic Teleop", group = "Example")
public class BasicTeleop extends OpMode {
    @Override
    public void init() {
        telemetry.addLine("Initialized");
    }

    @Override
    public void loop() {
        telemetry.addData("Loop time", getRuntime());
    }
}
