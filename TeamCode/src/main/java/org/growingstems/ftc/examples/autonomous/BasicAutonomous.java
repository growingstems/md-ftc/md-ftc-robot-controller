package org.growingstems.ftc.examples.autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

@Disabled
@Autonomous(name = "Basic Auto", group = "Example")
public class BasicAutonomous extends LinearOpMode {
    @Override
    public void runOpMode() {
        telemetry.addLine("Initialized");
        telemetry.update();

        waitForStart();

        while (opModeIsActive()) {
            telemetry.addData("Runtime", getRuntime());
            telemetry.update();
        }
    }
}
