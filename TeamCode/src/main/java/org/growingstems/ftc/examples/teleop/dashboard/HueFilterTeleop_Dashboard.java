package org.growingstems.ftc.examples.teleop.dashboard;

import com.acmerobotics.dashboard.FtcDashboard;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.growingstems.ftc.examples.pipelines.DashboardColorPickerPipeline;
import org.growingstems.ftc.examples.subsystems.ASyncWebcam;
import org.opencv.core.Core;
import org.openftc.easyopencv.OpenCvCameraRotation;

@Disabled
@TeleOp(name = "Hue Filter Dashboard", group = "Dashboard")
public class HueFilterTeleop_Dashboard extends OpMode {
    // Variables for using CV
    private final ASyncWebcam webcam = new ASyncWebcam("Webcam", OpenCvCameraRotation.UPRIGHT);
    private final DashboardColorPickerPipeline pipeline = new DashboardColorPickerPipeline(Core.ROTATE_90_COUNTERCLOCKWISE);

    @Override
    public void init() {
        // Initialize camera
        webcam.init(hardwareMap, pipeline);

        // Tell user we're done
        telemetry.addLine("Initialized");
    }

    @Override
    public void start() {
        FtcDashboard.getInstance().startCameraStream(pipeline, 0);
        super.start();
    }

    @Override
    public void loop() {
        telemetry.addData("Loop time", getRuntime());
    }

    @Override
    public void stop() {
        FtcDashboard.getInstance().stopCameraStream();
        super.stop();
    }
}
