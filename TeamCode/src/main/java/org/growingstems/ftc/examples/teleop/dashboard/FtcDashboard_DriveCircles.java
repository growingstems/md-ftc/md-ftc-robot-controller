package org.growingstems.ftc.examples.teleop.dashboard;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.growingstems.ftc.library.PolygonUtils;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;
import org.growingstems.measurements.Angle;

@Disabled
@TeleOp(name = "DriveCircles", group = "Dashboard")
public class FtcDashboard_DriveCircles extends OpMode {
    private FtcDashboard dashboard;

    @Disabled
    @Config
    public static class CircleParams {
        public static double centerX = 0.0;
        public static double centerY = 0.0;
        public static double radius = 20.0;
        public static double secondsPerRotation = 10.0;
    }

    @Disabled
    @Config
    public static class RobotParams {
        public static double width = 18.0;
        public static double length = 18.0;
    }

    @Override
    public void init() {
        dashboard = FtcDashboard.getInstance();

        TelemetryPacket packet = new TelemetryPacket();
        packet.put("Status", "Initialized");
        packet.fieldOverlay()
                .setFill("blue")
                .fillRect(CircleParams.centerX, CircleParams.centerY, RobotParams.width, RobotParams.length);

        dashboard.sendTelemetryPacket(packet);

        telemetry.addLine("Initialized");
    }

    @Override
    public void loop() {
        double runtime = getRuntime();
        Angle heading = Angle.rotations(runtime / CircleParams.secondsPerRotation);
        Vector2d centerRotation = new Vector2d(CircleParams.centerX, CircleParams.centerY);
        Vector2d robotCenter = new Vector2d(CircleParams.radius, heading).add(centerRotation);
        PolygonUtils.Polygon rectangle = PolygonUtils.makeRectangle(new Pose2d(robotCenter, heading), RobotParams.length, RobotParams.width);

        TelemetryPacket packet = new TelemetryPacket();
        packet.put("Status", "Running");
        packet.fieldOverlay()
                .setFill("blue")
                .fillPolygon(rectangle.xPoints, rectangle.yPoints);

        dashboard.sendTelemetryPacket(packet);

        telemetry.addLine("Running");
    }
}
