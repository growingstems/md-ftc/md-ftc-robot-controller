## Pipelines
Location for Computer Vision Pipelines.

Pipelines must extend `OpenCvPipeline`.
Classes which are used for Computer Vision that do not extend `OpenCvPipeline` may also go here, but it is worth considering a different location for such classes.