package org.growingstems.ftc.examples.teleop;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.growingstems.ftc.examples.pipelines.DashboardColorPickerPipeline;
import org.growingstems.ftc.examples.subsystems.ASyncWebcam;
import org.growingstems.logic.LogicCore;
import org.growingstems.signals.EdgeDetector;
import org.openftc.easyopencv.OpenCvCameraRotation;

import java.util.function.Supplier;

@Disabled
@TeleOp(name = "Hue Filter", group = "Computer Vision")
public class HueFilterTeleop extends OpMode {
    // Variables for using CV
    private final DashboardColorPickerPipeline pipeline = new DashboardColorPickerPipeline();
    private final ASyncWebcam webcam = new ASyncWebcam("Webcam", OpenCvCameraRotation.UPRIGHT);
    private double delta = 5.0;

    private enum ValueSelection {
        HUE,
        SATURATION,
        VALUE
    }

    private enum TypeSelection {
        MIN,
        MAX
    }

    private ValueSelection valueSelection = ValueSelection.HUE;
    private TypeSelection typeSelection = TypeSelection.MIN;

    // Variables for controlling CV live
    private final Supplier<Boolean> increment = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.dpad_up);
    private final Supplier<Boolean> decrement = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.dpad_down);
    private final Supplier<Boolean> incrementValue = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.y);
    private final Supplier<Boolean> decrementValue = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.a);
    private final Supplier<Boolean> incrementType = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.b);
    private final Supplier<Boolean> decrementType = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.x);
    private final Supplier<Boolean> incrementDelta = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.dpad_right);
    private final Supplier<Boolean> decrementDelta = new EdgeDetector(LogicCore.Edge.RISING).provide(() -> gamepad1.dpad_left);

    @Override
    public void init() {
        webcam.init(hardwareMap, pipeline);

        // Tell user we're done
        telemetry.addLine("Initialized");
    }

    @Override
    public void loop() {
        if (incrementDelta.get()) {
            delta++;
        }
        if (decrementDelta.get()) {
            delta--;
        }
        if (incrementType.get() || decrementType.get()) {
            switch (typeSelection) {
                case MAX:
                    typeSelection = TypeSelection.MIN;
                    break;
                case MIN:
                    typeSelection = TypeSelection.MAX;
                    break;
            }
        }
        if (incrementValue.get()) {
            switch (valueSelection) {
                case HUE:
                    valueSelection = ValueSelection.SATURATION;
                    break;
                case SATURATION:
                    valueSelection = ValueSelection.VALUE;
                    break;
                case VALUE:
                    valueSelection = ValueSelection.HUE;
                    break;
            }
        }
        if (decrementValue.get()) {
            switch (valueSelection) {
                case HUE:
                    valueSelection = ValueSelection.VALUE;
                    break;
                case SATURATION:
                    valueSelection = ValueSelection.HUE;
                    break;
                case VALUE:
                    valueSelection = ValueSelection.SATURATION;
                    break;
            }
        }
        if (increment.get()) {
            switch (valueSelection) {
                case HUE:
                    switch (typeSelection) {
                        case MIN:
                            pipeline.setMinHue(pipeline.getMinHue() + delta);
                            break;
                        case MAX:
                            pipeline.setMaxHue(pipeline.getMaxHue() + delta);
                            break;
                    }
                    break;
                case SATURATION:
                    switch (typeSelection) {
                        case MIN:
                            pipeline.setMinSaturation(pipeline.getMinSaturation() + delta);
                            break;
                        case MAX:
                            pipeline.setMaxSaturation(pipeline.getMaxSaturation() + delta);
                            break;
                    }
                    break;
                case VALUE:
                    switch (typeSelection) {
                        case MIN:
                            pipeline.setMinValue(pipeline.getMinValue() + delta);
                            break;
                        case MAX:
                            pipeline.setMaxValue(pipeline.getMaxValue() + delta);
                            break;
                    }
                    break;
            }
        }
        if (decrement.get()) {
            switch (valueSelection) {
                case HUE:
                    switch (typeSelection) {
                        case MIN:
                            pipeline.setMinHue(pipeline.getMinHue() - delta);
                            break;
                        case MAX:
                            pipeline.setMaxHue(pipeline.getMaxHue() - delta);
                            break;
                    }
                    break;
                case SATURATION:
                    switch (typeSelection) {
                        case MIN:
                            pipeline.setMinSaturation(pipeline.getMinSaturation() - delta);
                            break;
                        case MAX:
                            pipeline.setMaxSaturation(pipeline.getMaxSaturation() - delta);
                            break;
                    }
                    break;
                case VALUE:
                    switch (typeSelection) {
                        case MIN:
                            pipeline.setMinValue(pipeline.getMinValue() - delta);
                            break;
                        case MAX:
                            pipeline.setMaxValue(pipeline.getMaxValue() - delta);
                            break;
                    }
                    break;
            }
        }

        // Report to user the current hue range and runtime
        telemetry.addData("Selection", typeSelection + " " + valueSelection);
        telemetry.addData("Delta", delta);
        telemetry.addData("Min Hue", pipeline.getMinHue());
        telemetry.addData("Max Hue", pipeline.getMaxHue());
        telemetry.addData("Min Saturation", pipeline.getMinSaturation());
        telemetry.addData("Max Saturation", pipeline.getMaxSaturation());
        telemetry.addData("Min Value", pipeline.getMinValue());
        telemetry.addData("Max Value", pipeline.getMaxValue());
        telemetry.addData("Loop time", getRuntime());
    }
}
