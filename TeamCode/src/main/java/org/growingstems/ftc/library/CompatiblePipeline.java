package org.growingstems.ftc.library;

import android.graphics.Bitmap;

import org.firstinspires.ftc.robotcore.external.function.Consumer;
import org.firstinspires.ftc.robotcore.external.function.Continuation;
import org.firstinspires.ftc.robotcore.external.function.ContinuationResult;
import org.firstinspires.ftc.robotcore.external.stream.CameraStreamSource;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.openftc.easyopencv.OpenCvPipeline;

public abstract class CompatiblePipeline extends OpenCvPipeline implements CameraStreamSource {
    private final Integer rotateCode;
    private Bitmap latestImage;

    public CompatiblePipeline(Integer rotateCode) {
        this.rotateCode = rotateCode;
    }

    @Override
    public void getFrameBitmap(Continuation<? extends Consumer<Bitmap>> continuation) {
        continuation.dispatch(new ContinuationResult<Consumer<Bitmap>>() {
            @Override
            public void handle(Consumer<Bitmap> bitmapConsumer) {
                bitmapConsumer.accept(latestImage);
            }
        });
    }

    @Override
    public Mat processFrame(Mat input) {
        Mat output = processImage(input);
        if (latestImage == null) {
            if (rotateCode == null || rotateCode == Core.ROTATE_180) {
                // No rotation or 180 rotation
                latestImage = Bitmap.createBitmap(output.cols(), output.rows(), Bitmap.Config.ARGB_8888);
            } else {
                // 90 degree rotation either direction
                latestImage = Bitmap.createBitmap(output.rows(), output.cols(), Bitmap.Config.ARGB_8888);
            }
        }
        if (rotateCode == null) {
            // No Rotation - Just Convert
            Utils.matToBitmap(output, latestImage);
        } else {
            // Rotate then Convert
            Mat rotated = new Mat();
            Core.rotate(output, rotated, rotateCode);
            Utils.matToBitmap(rotated, latestImage);
            rotated.release();
        }
        return output;
    }

    protected abstract Mat processImage(Mat input);
}
