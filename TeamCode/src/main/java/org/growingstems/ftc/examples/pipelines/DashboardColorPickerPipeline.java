package org.growingstems.ftc.examples.pipelines;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.util.Range;

import org.growingstems.ftc.library.CompatiblePipeline;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

@Disabled
@Config
public class DashboardColorPickerPipeline extends CompatiblePipeline {
    public static double minHue = 20.0;
    public static double maxHue = 35.0;
    public static double minSaturation = 100.0;
    public static double maxSaturation = 255.0;
    public static double minValue = 100.0;
    public static double maxValue = 255.0;

    private final Scalar adder = new Scalar(-255.0, -255.0, -255.0);

    public DashboardColorPickerPipeline() {
        this(null);
    }

    public DashboardColorPickerPipeline(Integer rotateCode) {
        super(rotateCode);
    }

    @Override
    public Mat processImage(Mat input) {
        // Convert to HSV
        Mat hsv = new Mat();
        Imgproc.cvtColor(input, hsv, Imgproc.COLOR_RGB2HSV);

        // Apply HSV filter
        Mat filtered = new Mat();
        Core.inRange(hsv,
                new Scalar(minHue, minSaturation, minValue),
                new Scalar(maxHue, maxSaturation, maxValue),
                filtered);

        // Draw adder over pixels that pass filter.
        Core.add(input, adder, input, filtered);

        // Release unused mats and return image to display
        hsv.release();
        filtered.release();
        return input;
    }

    public double getMinHue() {
        return minHue;
    }

    public void setMinHue(double minHue) {
        DashboardColorPickerPipeline.minHue = Range.clip(minHue, 0.0, 180.0);
    }

    public double getMaxHue() {
        return maxHue;
    }

    public void setMaxHue(double maxHue) {
        DashboardColorPickerPipeline.maxHue = Range.clip(maxHue, 0.0, 180.0);
    }

    public double getMinSaturation() {
        return minSaturation;
    }

    public void setMinSaturation(double minSaturation) {
        DashboardColorPickerPipeline.minSaturation = Range.clip(minSaturation, 0.0, 255.0);
    }

    public double getMaxSaturation() {
        return maxSaturation;
    }

    public void setMaxSaturation(double maxSaturation) {
        DashboardColorPickerPipeline.maxSaturation = Range.clip(maxSaturation, 0.0, 255.0);
    }

    public double getMinValue() {
        return minValue;
    }

    public void setMinValue(double minValue) {
        DashboardColorPickerPipeline.minValue = Range.clip(minValue, 0.0, 255.0);
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        DashboardColorPickerPipeline.maxValue = Range.clip(maxValue, 0.0, 255.0);
    }
}
