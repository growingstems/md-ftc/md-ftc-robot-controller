package org.growingstems.ftc.library;

import org.growingstems.math.Pose2d;
import org.growingstems.measurements.Angle;

public class PolygonUtils {

    public static class Polygon {
        public double[] xPoints;
        public double[] yPoints;
    }

    /**
     * Create an angled rectangle
     *
     * The Pose Angle is CCW-Positive. If zero, the length is in the x-direction, and the width is
     * in the y-direction.
     *
     * @param pose The pose of the rectangle
     * @param length The length of the rectangle
     * @param width The width of the rectangle
     *
     * @return A polygon representing the described rectangle
     */
    public static Polygon makeRectangle(Pose2d pose, double length, double width) {
        // Adjust width/length to be from center of rectangle
        width *= 0.5;
        length *= 0.5;

        double xCenter = pose.getX();
        double yCenter = pose.getY();
        Angle heading = pose.getRotation();

        Polygon rectangle = new Polygon();
        rectangle.xPoints = new double[] { xCenter, xCenter, xCenter, xCenter };
        rectangle.yPoints = new double[] { yCenter, yCenter, yCenter, yCenter };

        double widthCos = width * heading.cos();
        double widthSin = width * heading.sin();
        double heightCos = length * heading.cos();
        double heightSin = length * heading.sin();

        rectangle.xPoints[0] += widthCos - heightSin;
        rectangle.yPoints[0] += widthSin + heightCos;

        rectangle.xPoints[1] -= widthCos + heightSin;
        rectangle.yPoints[1] -= widthSin - heightCos;

        rectangle.xPoints[2] -= widthCos - heightSin;
        rectangle.yPoints[2] -= widthSin + heightCos;

        rectangle.xPoints[3] += widthCos + heightSin;
        rectangle.yPoints[3] += widthSin - heightCos;

        return rectangle;
    }
}
