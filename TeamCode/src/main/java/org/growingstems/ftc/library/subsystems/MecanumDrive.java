package org.growingstems.ftc.library.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.growingstems.math.Vector2d;

import java.util.Arrays;

public class MecanumDrive {
    private final double inPerSuForwards;
    private final double inPerSuStrafe;

    private final DcMotor backLeft;
    private final DcMotor backRight;
    private final DcMotor frontLeft;
    private final DcMotor frontRight;

    public MecanumDrive(double inPerSuForwards,
                        double inPerSuStrafe,
                        DcMotor backLeft,
                        DcMotor backRight,
                        DcMotor frontLeft,
                        DcMotor frontRight) {
        this.inPerSuForwards = inPerSuForwards;
        this.inPerSuStrafe = inPerSuStrafe;
        this.backLeft = backLeft;
        this.backRight = backRight;
        this.frontLeft = frontLeft;
        this.frontRight = frontRight;
    }

    public void setZeroPowerBehavior(DcMotor.ZeroPowerBehavior zeroPowerBehavior){
        backLeft.setZeroPowerBehavior(zeroPowerBehavior);
        backRight.setZeroPowerBehavior(zeroPowerBehavior);
        frontLeft.setZeroPowerBehavior(zeroPowerBehavior);
        frontRight.setZeroPowerBehavior(zeroPowerBehavior);
    }

    public void move(double forwardsPower, double strafePower, double turnPower) {
        move(forwardsPower, strafePower, turnPower, 1.0);
    }

    public void move(double forwardsPower, double strafePower, double turnPower, double maxPowerAllowed){
        double backLeftPower = forwardsPower + strafePower - turnPower;
        double backRightPower = forwardsPower - strafePower + turnPower;
        double frontLeftPower = forwardsPower - strafePower - turnPower;
        double frontRightPower = forwardsPower + strafePower + turnPower;

        double largestPower = 0.0;
        for (double power : Arrays.asList(backLeftPower, backRightPower, frontLeftPower, frontRightPower)) {
            power = Math.abs(power);
            largestPower = Math.max(power, largestPower);
        }

        if (largestPower > maxPowerAllowed) {
            double multiplier = maxPowerAllowed / largestPower;
            backLeftPower *= multiplier;
            backRightPower *= multiplier;
            frontLeftPower *= multiplier;
            frontRightPower *= multiplier;
        }

        backLeft.setPower(backLeftPower);
        backRight.setPower(backRightPower);
        frontLeft.setPower(frontLeftPower);
        frontRight.setPower(frontRightPower);
    }

    public Vector2d getRelativePosition() {
        int BLPos = backLeft.getCurrentPosition();
        int BRPos = backRight.getCurrentPosition();
        int FLPos = frontLeft.getCurrentPosition();
        int FRPos = frontRight.getCurrentPosition();

        double xBar = inPerSuForwards * (BLPos + BRPos + FLPos + FRPos) / 4.0;
        double yBar = inPerSuStrafe * (BLPos - BRPos - FLPos + FRPos) / 4.0;
        return new Vector2d(xBar, yBar);
    }

    public void printEncoders(Telemetry telemetry) {
        telemetry.addData("Front Left", frontLeft.getCurrentPosition());
        telemetry.addData("Front Right", frontRight.getCurrentPosition());
        telemetry.addData("Back Left", backLeft.getCurrentPosition());
        telemetry.addData("Back Right", backRight.getCurrentPosition());
    }
}
