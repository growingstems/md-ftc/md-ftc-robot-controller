package org.growingstems.ftc.library;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.growingstems.signals.api.SignalModifier;

public class AccelerationController implements SignalModifier<Double, Double> {
    private double currentPower = 0.0;
    private ElapsedTime timer = null;
    private final double maxAcceleration_powerPerSecond;

    public AccelerationController(double maxAcceleration_powerPerSecond) {
        this.maxAcceleration_powerPerSecond = maxAcceleration_powerPerSecond;
    }

    @Override
    public Double update(Double in) {
        if (timer == null) {
            timer = new ElapsedTime();
        } else {
            double maxDelta = timer.seconds() * maxAcceleration_powerPerSecond;
            timer.reset();

            if (in > 0.0) {
                if (in - currentPower > maxDelta) {
                    currentPower += maxDelta;
                } else {
                    currentPower = in;
                }
            } else {
                if (currentPower - in > maxDelta) {
                    currentPower -= maxDelta;
                } else {
                    currentPower = in;
                }
            }
        }
        return currentPower;
    }
}
