package org.growingstems.ftc.examples.subsystems;

import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;
import org.openftc.easyopencv.OpenCvWebcam;

public class ASyncWebcam {
    private final String cameraName;
    private final OpenCvCameraRotation rotation;
    private final int width;
    private final int height;
    private OpenCvWebcam webcam;
    private Status status = Status.UNINITIALIZED;
    private int lastErrorCode;

    public enum Status {
        UNINITIALIZED,
        INITIALIZING,
        RUNNING,
        ERROR
    }

    public ASyncWebcam(String cameraName, OpenCvCameraRotation rotation) {
        this(cameraName, rotation, 320, 240);
    }

    public ASyncWebcam(String cameraName, OpenCvCameraRotation rotation, int width, int height) {
        this.cameraName = cameraName;
        this.rotation = rotation;
        this.width = width;
        this.height = height;
    }

    public void init(HardwareMap hardwareMap, OpenCvPipeline pipeline) {
        init(hardwareMap, pipeline, 2500);
    }

    public void init(HardwareMap hardwareMap, OpenCvPipeline pipeline, int msTimeout) {
        status = Status.INITIALIZING;
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, cameraName), cameraMonitorViewId);
        webcam.setPipeline(pipeline);
        webcam.setMillisecondsPermissionTimeout(msTimeout);
        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener()
            {
                @Override
                public void onOpened() {
                    webcam.startStreaming(width, height, rotation);
                    status = Status.RUNNING;
                }

                @Override
                public void onError(int errorCode) {
                    status = Status.ERROR;
                    lastErrorCode = errorCode;
                }
            });
    }

    public int getLastErrorCode() {
        return lastErrorCode;
    }

    public Status getStatus() {
        return status;
    }
}
